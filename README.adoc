= AsciiDoc WG

This repository hosts the AsciiDoc brand assets as well as documents and resources that pertain to the AsciiDoc WG proceedings.
To learn more about the AsciiDoc WG and its mission, visit the https://asciidoc-wg.eclipse.org/[AsciiDoc WG site].

== Trademarks

AsciiDoc(R) is a registered trademark of the Eclipse Foundation, Inc.
